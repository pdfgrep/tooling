# Ubuntu Dockerfile

This is a dockerfile for the latest ubuntu release. Its roughly what
the continuous integration on GitLab uses.

## Usage

Run in this directory:

    docker build -t pdfgrep-ubuntu .
    docker run -v /path/to/pdfgrep/source:/home/pdfgrep pdfgrep-ubuntu
