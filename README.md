# pdfgrep tooling

This repository contains various configuration files that can be used
to create containers/virtual machines with different operating systems
to test [pdfgrep][pdfgrep]. This is part of the release procedure of [pdfgrep][pdfgrep].

See the READMEs in the individual sub-directories for more
information.

[pdfgrep]: https://pdfgrep.org
