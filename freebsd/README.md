# FreeBSD Vagrantfile

This is a Vagrant file that allows to easily create a virtual machine
that runs FreeBSD and has all the build dependencies for pdfgrep
installed.

# Usage

Edit the `config.vm.synced_folder` property in the Vagrantfile so that
the source directory of pdfgrep is synced to `/vagrant` in the VM.
Then run in this directory:

    vagrant up
    vagrant ssh -c "cd /vagrant; ./autogen.sh && ./configure && make check"

You can stop the VM again with `vagrant halt` and destroy it with
`vagrant destroy`.
