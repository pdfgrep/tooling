# Debian Dockerfile

This is a dockerfile for debian jessie that installs all build and
test dependencies of pdfgrep. It exists mainly to test if pdfgrep's
tests work with poppler < 3.34, which has a bug that requires some
tests to be disabled.

## Usage

Run in this directory:

    docker build -t pdfgrep-debian .
    docker run -v /path/to/pdfgrep/source:/home/pdfgrep pdfgrep-debian
